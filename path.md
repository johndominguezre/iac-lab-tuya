#  Creación de vm con terraform
## Ingresar a vm configurada en sesión de prerrequisitos
## Revisar que tengas todas la herramientas necesarias en el servidore remoto
- terraform --version
- git --version
- ansible --version
- az --version
### Nota si te falta alguna instalación en la carpeta de scripts estan los instaladores necesarios
## Crear un repo en tu cuenta de azure devops con el nombre iac-lab-tuya importa o clona el siguiente repo
https://johndominguezre@bitbucket.org/johndominguezre/iac-lab-tuya.git
## Clona el repo en tu local y abrelo con vscode para mejor visualización
## Clona el repo en su servidor remoto 

## - Ingresa azure portal 
az login -- analizar output
- Ingresar url y código en el navegador --analizar output
# - Crear el rbac
az ad sp create-for-rbac -n "name-rbac" --role contributor --scopes /subscriptions/<subscription-id>
-- analizar output
#Extrae los siguientes datos pegalos en este espacio
//////////////////

//////////////////
# En vscode abre la carpeta node-app-server/terraform y revisa cada uno de sus componentes
# Remplaza los valores del rbac creado en las variables y la llave ssh publica
# Crea un storage en azure portal para guardar el estado de la infraestructura
# Ingresa los datos en el archivo backend.tf
# Agrega tu nombre en la parte final del domain name
# Realiza push de tus cambios y luego pull en el servidor remoto
#Incia terraform y  Aplica los cambios a la infraestructure definida  - En el servidor remoto
terraform init
terraform apply en diferentes folder y luego en el que contiene los .tf
# Prueba algunos comandos mas utilizados -- analiza la salida de cada comando
    apply              Builds or changes infrastructure
    console            Interactive console for Terraform interpolations
    destroy            Destroy Terraform-managed infrastructure
    fmt                Rewrites config files to canonical format
    init               Initialize a Terraform working directory
    plan               Generate and show an execution plan
    providers          Prints a tree of the providers used in the configuration
    show               Inspect Terraform state or plan
    validate           Validates the Terraform files
    version            Prints the Terraform version
#Revisa en el portal la creación de los recursos
#Analiza  el terraform state en el storage
#Ahora Crea un politica de acceso par permitir el acceso por el puerto 9090 en el local y luego push en el server remoto usuando vscode dale push a tus cambios y luego pull en el servidor remoto
#Revisa los resultados
#Destruye la infraestructura creada
terraform destroy
# Ahora crea la infraestructura completa 
# Conectate al servidor que creaste por medio de ssh
###################################################################
## ANSIBLE
# Ingresa a la carpeta ansible y revisa los componentes creados

# En los hosts agrega el dominio de tu servidor y ejecuta el playbook
ansible playbook <yml file>
#Revisa en tu dominio los puertos e información con tu navegador
# Ingresa a la ruta /var/www/html/example-80 y modifica el index.html
# Revisa el cambio en el navegador y ejecuta nuevamente el playbook
####################################################################
## Build Agent
#Es hora de Crear nuestro propio build agent
#Realiza los cambios de variables en los mismos archivos
#Ejecuta la ejecución de terraform
#Crea el Personal access token y remplaza los datos en el archivo de ansible
- Profile - personal access token darel full access 
y remplazar los datos en el linuxagent
#Crear el pool Agent en azure devops con este nombre
Hosted-Tuya-Lab Ubuntu Example VM
#Ejecuta el playbook y valida la ejecución correcta con el registro del nuevo agente en azure
#Agregaga las tereas necesarias para que funcionen todas las etapas del pipeline





