#/bin/bash
#Get packages and certificates
echo "Update systems and Installing certificates"
sudo apt-get update
sudo apt-get install ca-certificates curl apt-transport-https lsb-release gnupg --yes
#Download and installing Microsoft signing key
echo "Download and installing Microsoft signing key"
curl -sL https://packages.microsoft.com/keys/microsoft.asc | 
    gpg --dearmor | 
    sudo tee /etc/apt/trusted.gpg.d/microsoft.asc.gpg > /dev/null
#Add the az CLI software repository
echo "Add the az CLI software repository"
AZ_REPO=$(lsb_release -cs)
echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | 
    sudo tee /etc/apt/sources.list.d/azure-cli.list
#Update repository and install az cli
echo "Update repository and install az cli"
sudo apt-get update
sudo apt-get install azure-cli --yes
#Show Verssion az cli
az --version
