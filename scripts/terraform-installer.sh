#/bin/bash/
#Updating packages
sudo apt update --yes
# Installing unzip package
echo "Installing unzip"
sudo apt install unzip
wget https://releases.hashicorp.com/terraform/0.12.18/terraform_0.12.18_linux_amd64.zip
unzip *.zip
sudo mv terraform /usr/local/bin
