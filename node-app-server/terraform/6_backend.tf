terraform {
  backend "azurerm" {
    storage_account_name = "terraformstatelabtuya"
    container_name       = "states"
    key                  = "terraform.tfstate"
    access_key           = ""
  }
}
