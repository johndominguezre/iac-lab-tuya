resource "azurerm_public_ip" "complete_example" {
  name                         = "example_vm_publicip"
  location                     = "${azurerm_resource_group.complete_example.location}"
  resource_group_name          = "${azurerm_resource_group.complete_example.name}"
  domain_name_label            = "${var.tag}"
  allocation_method             = "Static"

  tags = {
    group = "${var.tag}"
  }
}

resource "azurerm_network_interface" "complete_example" {
  name                      = "example_vm_interface"
  location                  = "${azurerm_resource_group.complete_example.location}"
  resource_group_name       = "${azurerm_resource_group.complete_example.name}"
  network_security_group_id = "${azurerm_network_security_group.complete_example.id}"

  ip_configuration {
    name                          = "example_vm_ip"
    subnet_id                     = "${azurerm_subnet.complete_example.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.complete_example.id}"
  }

  tags = {
    group = "${var.tag}"
  }
}