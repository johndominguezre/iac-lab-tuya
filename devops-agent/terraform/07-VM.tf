variable "vm_username" {
  description = "Enter admin username to ssh into Linux VM"
}

variable "vm_name" {
  description = "Enter admin username to ssh into Linux VM"
}

variable "vm_type" {
  description = "Enter the type of machine you want"
}


variable "admin_ssh_publickey" {
  type        = "string"
  description = "Configure with your public ssh key"
}

resource "azurerm_virtual_machine" "complete_example" {
  name                  = "${var.vm_name}"
  location              = "${azurerm_resource_group.complete_example.location}"
  resource_group_name   = "${azurerm_resource_group.complete_example.name}"
  network_interface_ids = ["${azurerm_network_interface.complete_example.id}"]
  vm_size               = "${var.vm_type}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
    delete_os_disk_on_termination = true


  # Uncomment this line to delete the data disks automatically when deleting the VM
    delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "${var.vm_name}"
    admin_username = "${var.vm_username}"
  }
  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.vm_username}/.ssh/authorized_keys"
      key_data = "${var.admin_ssh_publickey}"
    }
  }
  tags = {
    group = "${var.tag}"
  }
}