resource "azurerm_virtual_network" "complete_example" {
  name                = "tuya_vm_virtualnetwork"
  address_space       = ["${var.vnet_cidr}"]
  location            = "${azurerm_resource_group.complete_example.location}"
  resource_group_name = "${azurerm_resource_group.complete_example.name}"
  tags = {
    group = "${var.tag}"
  }
}

resource "azurerm_subnet" "complete_example" {
  name                 = "tuya_vm_subnet"
  address_prefix       = "${var.subnet_cidr}"
  virtual_network_name = "${azurerm_virtual_network.complete_example.name}"
  resource_group_name  = "${azurerm_resource_group.complete_example.name}"
}

