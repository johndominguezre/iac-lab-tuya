terraform {
  backend "azurerm" {
    storage_account_name = "terraformstatelabtuya"
    container_name       = "states"
    key                  = "terraform-agent.tfstate"
    access_key           = "<key-storage>"
  }
}
